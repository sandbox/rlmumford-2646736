<?php
/**
 * @file
 * rules_routine.views_default.inc
 */

/**
 * Implements hook_views_default_views().
 */
function rules_routine_views_default_views() {
  $views = array();

  // Scan this directory for any .view files
  $files = file_scan_directory(dirname(__FILE__).'/views', '/\.view$/', array('key' => 'name'));
  foreach ($files as $file) {
    if ((include $file->uri) == 1) {
      $views[$view->name] = $view;
    }
  }

  return $views;
}
