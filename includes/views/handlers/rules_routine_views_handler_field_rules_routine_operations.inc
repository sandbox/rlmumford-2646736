<?php

/**
 * Field handler to present a payment transaction's operations links.
 */
class rules_routine_views_handler_field_rules_routine_operations extends views_handler_field {
  function construct() {
    parent::construct();

    $this->additional_fields['id'] = 'id';
  }

  function query() {
    $this->ensure_my_table();
    $this->add_additional_fields();
  }

  function render($values) {
    $id = $this->get_value($values, 'id');
    $info = entity_get_info('rules_routine');
    $path = $info['admin ui']['path'];

    $links = menu_contextual_links('rules-routine', $path.'/routine/'.$id, array());

    if (!empty($links)) {
      return theme('links', array('links' => $links, 'attributes' => array('class' => array('links', 'inline', 'operations'))));
    }
  }
}
