<?php

class rules_routine_views_handler_field_rules_routine_export_status extends views_handler_field_numeric {

  public function render($values) {
    $value = $this->get_value($values);
    return array(
      '#theme' => 'entity_status',
      '#status' => $value,
    );
  }
}
