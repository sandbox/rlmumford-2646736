<?php
/**
 * @file
 * Entity property info for routines.
 */

/**
 * Implements hook_entity_info_alter().
 */
function rules_routine_entity_info_alter(&$info) {
  $props = &$info['rules_routine']['properties'];

  $props['next']['type'] = 'date';
  $props['status']['options list'] = 'rules_routine_status_options';
  $props['component']['type'] = 'rules_config';
}
