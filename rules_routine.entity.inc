<?php
/**
 * @file
 * Entity Classes for rules routine.
 */

class RulesRoutine extends Entity {

  public $auto_renew = FALSE;

  public function presave() {
    // Do nothing if queued.
    if ($this->status == 'queued') {
      return;
    }

    if (empty($this->status)) {
      $this->status = 'scheduled';
    }

    // Save the rrule.
    if (!empty($this->routine_datetime[LANGUAGE_NONE][0])) {
      $item = $this->routine_datetime[LANGUAGE_NONE][0];
      if (empty($item['rrule'])) {
        // If a routine has been scheduled without a repeating date then save
        // nothing in the rrule.
        $this->rrule = NULL;
      }
      else {
        $this->rrule = $item['rrule'];
      }
    }

    // Save the final.
    if (!empty($this->routine_datetime[LANGUAGE_NONE])) {
      $item = $this->routine_datetime[LANGUAGE_NONE][count($this->routine_datetime[LANGUAGE_NONE]) - 1];
      $this->final = $item['value'];
    }

    // Update the next date if required.
    $needs_updated_next = empty($this->next)
      || $this->routine_datetime[LANGUAGE_NONE][0]['value'] != $this->original->routine_datetime[LANGUAGE_NONE][0]['value'];
    if ($this->status == 'scheduled' && $needs_updated_next) {
      if (!empty($this->routine_datetime[LANGUAGE_NONE][0]['value'])) {
        // Set the next date to the first date in the list of values.
        $this->next = $this->routine_datetime[LANGUAGE_NONE][0]['value'];
      }
      else {
        $this->next = NULL;
        $this->status = 'expired';
      }
    }
  }

  public function execute($update_schedule = TRUE) {
    // Get the component
    $this->status = 'running';
    $this->save();

    try {
      $component = rules_config_load($this->component);
      if (empty($component)) {
        throw new Exception(t('Routine Component could not be loaded.'));
      }
      $component->execute($this);
    }
    catch (Exception $e) {
      $this->status = 'failed';
      $this->save();
      return;
    }

    // Reschedule the routine for the next date by unsetting next and saving.
    $this->status = 'scheduled';

    // If the $update_schedule flag has been set to false, do not change the
    // scheduling information.
    if (!$update_schedule) {
      $this->save();
      return;
    }

    // Work out the next scheduled date.
    $this->next = NULL;
    $removed_date_item = $this->routine_datetime[LANGUAGE_NONE][0];
    unset($this->routine_datetime[LANGUAGE_NONE][0]);
    $this->routine_datetime[LANGUAGE_NONE] = array_values($this->routine_datetime[LANGUAGE_NONE]);

    if (empty($this->routine_datetime[LANGUAGE_NONE])) {
      // Attempt to reschedule.
      // If we have the rrule and the final date of the routine then we can
      // attempt to auto renew the schedule.
      if (!empty($this->auto_renew) && !empty($this->rrule) && !empty($this->final)) {
        // Build a copy of the rrule that excludes the date that we have just
        // run.
        module_load_include('inc', 'date_api', 'date_api_ical');
        list($rrule, $exdate, $rdate) = date_repeat_split_rrule($this->rrule);
        $rrule['COUNT'] = 2;
        $next_rule = date_api_ical_build_rrule($rrule + array('EXDATE' => $exdate, 'RDATE' => $rdate));

        // Work out the next date by limiting the rrule to two dates and taking
        // the first one which isn't final.
        $new_item = array(
          'value' => $this->final,
          'rrule' => $next_rule,
          'timezone' => $removed_date_item['timezone'],
          'timezone_db' => $removed_date_item['timezone_db'],
          'date_type' => 'datetime',
        );
        $field = field_info_field('routine_datetime');
        $field['settings']['tz_handling'] = 'none';
        $dates = date_repeat_build_dates($next_rule, NULL, $field, $new_item);

        // Work out the new start date of this routine.
        $new_start = NULL;
        foreach ($dates as $date_item) {
          $new_start = $date_item['value'];

          if ($new_start != $this->final) {
            break;
          }
        }

        // Create a new fake item to generate the dates from.
        $new_item = array(
          'value' => $new_start,
          'rrule' => $this->rrule,
          'timezone' => $removed_date_item['timezone'],
          'timezone_db' => $removed_date_item['timezone_db'],
          'date_type' => 'datetime',
        );
        $this->routine_datetime[LANGUAGE_NONE] = date_repeat_build_dates($this->rrule, NULL, $field, $new_item);
      }
    }

    $this->save();
  }

  public function formatFinal($format = "Y-m-d \a\\t g:ia") {
    return $this->formatDate($this->final, $format);
  }

  public function formatNext($format = "Y-m-d \a\\t g:ia") {
    return $this->formatDate($this->next, $format);
  }

  protected function formatDate($date, $format = "Y-m-d \a\\t g:ia") {
    $utc = new DateTimeZone('UTC');
    $site_tz = new DateTimeZone(date_default_timezone(FALSE));
    $datetime = new DateTime($date, $utc);
    $datetime->setTimezone($site_tz);
    return $datetime->format($format);
  }
}

class RulesRoutineController extends EntityAPIControllerExportable {

  public function getComponentOptions($entity) {
    $components = rules_get_components(FALSE, 'action');
    $supported_types = array('entity', 'list<entity>', 'rules_routine', 'list<rules_routine>');
    $options = array();

    foreach ($components as $name => $component) {
      $params = $component->parameterInfo();
      $first_param = reset($params);

      if (!in_array($first_param['type'], $supported_types)) {
        continue;
      }

      if (isset($first_param['bundle']) && $first_param['bundle'] != $entity->type) {
        continue;
      }

      $options[$name] = $component->label;
    }

    return $options;
  }

  public function invoke($hook, $entity) {
    if ($hook == 'presave' && is_callable(array($entity, 'presave'))) {
      $entity->presave();
    }

    parent::invoke($hook, $entity);
  }
}

