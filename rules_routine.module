<?php
/**
 * @file
 * Module Code for Rules Routine
 */

/**
 * Implements hook_hook_info().
 */
function rules_routine_hook_info() {
  $hooks = array();
  $hooks['default_rules_routine_type'] = array('group' => 'rules_routine_type_default');
  $hooks['default_rules_routine_type_alter'] = array('group' => 'rules_routine_type_default');
  return $hooks;
}

/**
 * Implements hook_entity_info().
 */
function rules_routine_entity_info() {
  $info['rules_routine'] = array(
    'label' => t('Routine'),
    'description' => t('Rules Execution Routines'),
    'entity class' => 'RulesRoutine',
    'controller class' => 'RulesRoutineController',
    'base table' => 'rules_routine',
    'fieldable' => TRUE,
    'exportable' => TRUE,
    'entity keys' => array(
      'id' => 'id',
      'name' => 'name',
      'bundle' => 'type',
      'label' => 'label',
      'status' => 'export_status',
      'module' => 'export_module',
    ),
    'bundles' => array(),
    'bundle keys' => array(
      'bundle' => 'type',
    ),
    'module' => 'rules_routine',
    'access callback' => 'rules_routine_access',
    'admin ui' => array(
      'path' => 'admin/config/workflow/rules/routines',
      'file' => 'rules_routine.admin.inc',
      'controller class' => 'RulesRoutineUIController',
    ),
  );

  if (db_table_exists('rules_routine_type')) {
    $types = db_select('rules_routine_type', 't')
      ->fields('t')
      ->execute()
      ->fetchAllAssoc('type');
    foreach ($types as $bundle => $type) {
      $info['rules_routine']['bundles'][$bundle] = array(
        'label' => $type->label,
        'admin' => array(
          'path' => 'admin/structure/routine_types/manage/%rules_routine_type',
          'real path' => 'admin/structure/routine_types/manage/'.$bundle,
          'bundle argument' => 4,
          'access arguments' => array('administer rules_routine_types'),
        ),
      );
    }
  }

  $info['rules_routine_type'] = array(
    'label' => t('Routine Type'),
    'entity class' => 'Entity',
    'controller class' => 'EntityAPIControllerExportable',
    'base table' => 'rules_routine_type',
    'fieldable' => FALSE,
    'bundle of' => 'rules_routine',
    'exportable' => TRUE,
    'entity keys' => array(
      'id' => 'id',
      'name' => 'type',
      'label' => 'label',
    ),
    'access callback' => 'rules_routine_type_access',
    'module' => 'rules_routine',
    'admin ui' => array(
      'path' => 'admin/structure/routine_types',
      'file' => 'rules_routine_type.admin.inc',
      'controller class' => 'EntityDefaultUIController',
    ),
  );
  return $info;
}

/**
 * Implements hook_views_api().
 */
function rules_routine_views_api() {
  return array(
    'api' => '3',
  );
}

/**
 * Implements hook_permission().
 */
function rules_routine_permission() {
  return array(
    'administer rules_routine_types' => array(
      'title' => t('Administer Routine Types'),
    ),
    'administer rules_routines' => array(
      'title' => t('Administer Routines'),
    ),
  );
}

/**
 * Implements hook_cron().
 */
function rules_routine_cron() {
  $q = db_select('rules_routine', 'r');
  $q->condition('status', 'scheduled');
  $q->condition('next', gmdate('Y-m-d H:i:s', REQUEST_TIME), '<');
  $q->fields('r', array('id'));
  $ids = $q->execute()->fetchCol();

  $queue = DrupalQueue::get('rules_routine');
  foreach (entity_load('rules_routine', $ids) as $routine) {
    $routine->status = 'queued';
    $routine->save();

    $queue->createItem($routine->name);
  }
}

/**
 * Implements hook_cron_queue_info().
 */
function rules_routine_cron_queue_info() {
  $queues['rules_routine'] = array(
    'worker callback' => 'rules_routine_cron_execute',
    'time' => '15',
  );
  return $queues;
}

/**
 * Worker callback to execute routines.
 */
function rules_routine_cron_execute($routine_name) {
  if (!is_string($routine_name)) {
    return;
  }

  /** @var \RulesRoutine $routine */
  $routine = entity_load_single('rules_routine', $routine_name);
  if (!$routine || $routine->status != 'queued') {
    return;
  }

  $routine->execute();
}

/**
 * Implements hook_field_attach_create_bundle().
 */
function rules_routine_field_attach_create_bundle($entity_type, $bundle) {
  if ($entity_type != 'rules_routine') {
    return;
  }

  if (!field_info_field('routine_datetime')) {
    $field = array(
      'field_name' => 'routine_datetime',
      'type' => 'datetime',
      'cardinality' => FIELD_CARDINALITY_UNLIMITED,
      'settings' => array(
        'repeat' => 1,
      ),
    );
    field_create_field($field);
  }
  if (!field_info_instance($entity_type, 'routine_datetime', $bundle)) {
    $instance = array(
      'field_name' => 'routine_datetime',
      'entity_type' => $entity_type,
      'bundle' => $bundle,
      'label' => t('Routine Date/Time Rule'),
      'required' => TRUE,
      'widget' => array(
        'type' => 'date_popup',
        'settings' => array(
          'no_fieldset' => 1,
          'repeat_collapsed' => 0,
        ),
      ),
    );
    field_create_instance($instance);
  }
}

/**
 * Implements hook_fields_extra_fields().
 */
function rules_routine_field_extra_fields() {
  $extra = array();
  $info = entity_get_info('rules_routine');
  foreach ($info['bundles'] as $bundle => $bundle_info) {
    $extra['rules_routine'][$bundle] = array(
      'form' => array(
        'label' => array(
          'label' => t('Label'),
          'description' => t('Routine label.'),
          'weight' => -4,
        ),
        'name' => array(
          'label' => t('Machine Name'),
          'description' => t('Unique machine name'),
          'weight' => -3,
        ),
        'component' => array(
          'label' => t('Component'),
          'description' => t('The rule component this routine executes.'),
          'weight' => -1,
        ),
      ),
    );
  }
  return $extra;
}

/**
 * Load a rules_routine_type.
 */
function rules_routine_type_load($id) {
  return entity_load_single('rules_routine_type', $id);
}

/**
 * Access callback for rules_routine_types.
 */
function rules_routine_type_access() {
  return user_access('administer rules_routine_types');
}

/**
 * Load a rules_routine.
 */
function rules_routine_load($id) {
  return entity_load_single('rules_routine', $id);
}

/**
 * Access callback for rules routines.
 */
function rules_routine_access($op, $routine = NULL, $account = NULL, $entity_type = 'rules_routine') {
  if ($op == 'export') {
    // Do not allow the export if the component is custom.
    $rule = entity_load_single('rules_config', $routine->component);
    if (empty($rule) || $rule->status == 1) {
      return FALSE;
    }

    // Do not allow the export of routines that have entity reference fields.
    $instances = field_info_instances('rules_routine', $routine->type);
    $permitted_target_types = array('entity_template', 'ck_task_job');
    foreach ($instances as $field_name => $instance) {
      $field = field_info_field($field_name);
      if ($field['type'] == 'entityreference' && !in_array($field['settings']['target_type'], $permitted_target_types) && !empty($routine->{$field_name}[LANGUAGE_NONE])) {
        return FALSE;
      }
    }
  }

  return user_access('administer rules_routines', $account);
}

/**
 * Status options for routines.
 */
function rules_routine_status_options() {
  return array(
    'scheduled' => t('Scheduled'),
    'queued' => t('Queued'),
    'running' => t('Running'),
    'failed' => t('Failed'),
    'expired' => t('Expired'),
    'disabled' => t('Disabled'),
  );
}

/**
 * Implements hook_date_combo_process_alter().
 */
function rules_routine_date_combo_process_alter(&$element, &$form_state, $context) {
  if (empty($context['field']) || $context['field']['field_name'] != 'routine_datetime') {
    return;
  }

  $element['rrule']['#process'] = array('date_repeat_rrule_process', 'rules_routine_datetime_rrule_process');
}

/**
 * Process the datetime rrule element to add a 'forever' checkbox.
 */
function rules_routine_datetime_rrule_process($element, &$form_state, $form) {
  $element['range_of_repeat']['#options'] = array(
    'FOREVER' => t('Never'),
  ) + $element['range_of_repeat']['#options'];
  if (empty($element['#default_value'])) {
    $element['range_of_repeat']['#default_value'] = 'FOREVER';
  }
  else if ($form_state['routine']->auto_renew) {
    $element['range_of_repeat']['#default_value'] = 'FOREVER';
  }
  $element['range_of_repeat']['#element_validate'] = array('rules_routine_datetime_rrule_range_validate');
  $element['range_of_repeat'] = array(
    'forever_child' => array(
      '#markup' => t('Never (Run Indefinitely)'),
      '#weight' => -100,
    ),
  ) + $element['range_of_repeat'];
  return $element;
}

/**
 * Validate the range of repeat.
 */
function rules_routine_datetime_rrule_range_validate($element, &$form_state) {
  $parents = $element['#parents'];
  array_pop($parents);

  $values = &drupal_array_get_nested_value($form_state['values'], $parents);
  $input = &drupal_array_get_nested_value($form_state['input'], $parents);
  if ($values['range_of_repeat'] == 'FOREVER') {
    $values['range_of_repeat'] = 'COUNT';
    $input['range_of_repeat'] = 'COUNT';
    $values['count_child'] = 3;
    $input['count_child'] = 3;
    $form_state['values']['auto_renew'] = 1;
  }
  else {
    $form_state['values']['auto_renew'] = 0;
  }
}
