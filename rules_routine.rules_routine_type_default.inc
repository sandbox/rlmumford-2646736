<?php
/**
 * @file
 * Module Code for Rules Routine
 */

/**
 * Implements hook_default_rules_routine_type().
 */
function rules_routine_default_rules_routine_type() {
  $types = array();
  $types['default'] = entity_create('rules_routine_type', array(
    'label' => 'Default',
    'type' => 'default',
  ));
  return $types;
}