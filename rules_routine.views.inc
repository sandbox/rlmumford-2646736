<?php
/**
 * @file
 * Views integration.
 */

/**
 * Implements hook_views_data_alter().
 */
function rules_routine_views_data_alter(&$data) {
  $data['rules_routine']['export_status']['title'] = t('Export Status');
  $data['rules_routine']['export_status']['field']['handler'] = 'rules_routine_views_handler_field_rules_routine_export_status';
  $data['rules_routine']['operations'] = array(
    'field' => array(
      'title' => t('Operation Links'),
      'help' => t('Display all operation links for this routine.'),
      'handler' => 'rules_routine_views_handler_field_rules_routine_operations',
    ),
  );
}
