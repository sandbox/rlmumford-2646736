<?php
/**
 * @file
 * Rule Routine Type Admin Code.
 */

/**
 * Rule routine form.
 */
function rules_routine_type_form($form, &$form_state, $rules_routine_type, $op = 'edit') {

  if ($op == 'clone') {
    $rules_routine_type->label .= ' (cloned)';
    $rules_routine_type->type = '';
  }

  $form['label'] = array(
    '#title' => t('Label'),
    '#type' => 'textfield',
    '#default_value' => $rules_routine_type->label,
    '#description' => t('The human-readable name of this routine type.'),
    '#required' => TRUE,
    '#size' => 30,
  );
  // Machine-readable type name.
  $form['type'] = array(
    '#type' => 'machine_name',
    '#default_value' => isset($rules_routine_type->type) ? $rules_routine_type->type : '',
    '#maxlength' => 32,
    '#disabled' => !$rules_routine_type->is_new && $op != 'clone',
    '#machine_name' => array(
      'exists' => 'rules_routine_type_load',
      'source' => array('label'),
    ),
    '#description' => t('A unique machine-readable name for this routine type. It must only contain lowercase letters, numbers, and underscores.'),
  );

  $form['actions'] = array('#type' => 'actions');
  $form['actions']['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Save routine type'),
    '#weight' => 40,
  );

  return $form;
}

/**
 * Submit rules routine form.
 */
function rules_routine_type_form_submit($form, &$form_state) {
  $type = entity_ui_form_submit_build_entity($form, $form_state);
  // Save and go back.
  $type->save();
  $form_state['redirect'] = 'admin/structure/routine_types';
}
