<?php
/**
 * @file
 * Admin Code for Rules Routines.
 */

/**
 * UI Class.
 */
class RulesRoutineUIController extends EntityDefaultUIController {

  /**
   * {@inheritdoc}
   */
  public function hook_menu() {
    $items = array();
    $id_count = count(explode('/', $this->path));
    $wildcard = isset($this->entityInfo['admin ui']['menu wildcard']) ? $this->entityInfo['admin ui']['menu wildcard'] : '%'.$this->entityType;

    $items[$this->path] = array(
      'title' => t('Routines'),
      'description' => t('Add edit and update routines.'),
      'page callback' => 'system_admin_menu_block_page',
      'access arguments' => array('administer rules_routines'),
      'type' => MENU_LOCAL_TASK,
      'file path' => drupal_get_path('module', 'system'),
      'file' => 'system.admin.inc',
    );

    $items[$this->path.'/add'] = array(
      'title' => t('Add Routine'),
      'page callback' => 'rules_routine_add_page',
      'page arguments' => array(),
      'access callback' => 'user_access',
      'access arguments' => array('administer rules_routines'),
      'type' => MENU_LOCAL_ACTION,
      'file' => 'rules_routine.admin.inc',
      'file path' => drupal_get_path('module', 'rules_routine'),
    );

    foreach ($this->entityInfo['bundles'] as $bundle => $info) {
      $items[$this->path.'/add/'.$bundle] = array(
        'title' => t('Add a !bundle Routine', array('!bundle' => $info['label'])),
        'description' => t('Add a new Routine'),
        'page callback' => 'rules_routine_add_form_wrapper',
        'page arguments' => array($bundle),
        'type' => MENU_NORMAL_ITEM,
        'position' => 'right',
      ) + $this->menuItemDefaults($bundle, 'create');
    }

    $items[$this->path.'/routine/%rules_routine'] = array(
      'title callback' => 'entity_label',
      'title arguments' => array('rules_routine', $id_count + 1),
    ) + $this->menuItemDefaults($id_count + 1);
    $items[$this->path.'/routine/%rules_routine/edit'] = array(
      'title' => t('Edit'),
      'type' => MENU_DEFAULT_LOCAL_TASK,
      'context' => MENU_CONTEXT_PAGE | MENU_CONTEXT_INLINE,
    ) + $this->menuItemDefaults($id_count + 1);
    $items[$this->path.'/routine/%rules_routine/clone'] = array(
      'title' => t('Clone'),
      'type' => MENU_LOCAL_TASK,
      'context' => MENU_CONTEXT_PAGE | MENU_CONTEXT_INLINE,
    ) + $this->menuItemDefaults($id_count + 1, 'clone');
    $items[$this->path.'/routine/%rules_routine/delete'] = array(
      'title' => t('Delete'),
      'type' => MENU_LOCAL_TASK,
      'context' => MENU_CONTEXT_PAGE | MENU_CONTEXT_INLINE,
    ) + $this->menuItemDefaults($id_count + 1, 'delete');
    $items[$this->path.'/routine/%rules_routine/export'] = array(
      'title' => t('Export'),
      'type' => MENU_LOCAL_TASK,
      'context' => MENU_CONTEXT_PAGE | MENU_CONTEXT_INLINE,
    ) + $this->menuItemDefaults($id_count + 1, 'export');

    return $items;
  }

  /**
   * Get the defaults for a menu item
   */
  protected function menuItemDefaults($arg_pos, $op = 'edit') {
    return array(
      'page callback' => 'drupal_get_form',
      'page arguments' => array('rules_routine_form', $arg_pos, $op),
      'access callback' => 'rules_routine_access',
      'access arguments' => array($op, $arg_pos),
      'file' => 'rules_routine.admin.inc',
      'file path' => drupal_get_path('module', 'rules_routine'),
    );
  }

}

/**
 * Rules routine add page.
 */
function rules_routine_add_page() {
  $types = entity_load('rules_routine_type');
  $links = array();

  foreach ($types as $type) {
    $path = 'admin/config/workflow/rules/routines/add/'.$type->type;

    if (!drupal_valid_path($path)) {
      continue;
    }

    $link = array(
      'title' => t('Add a !bundle Routine', array('!bundle' => $type->label)),
      'description' => t('Add a new Routine'),
      'href' => $path,
      'localized_options' => array(
        'attributes' => array(),
      ),
    );
    $links[$type->type] = $link;
  }

  if (count($links) == 1) {
    $link = reset($links);
    drupal_goto($link['href']);
  }
  else if (!empty($links)) {
    return theme('admin_block_content', array('content' => $links));
  }
  else {
    return t('You do not have any administrative items.');
  }
}

/**
 * Rules routine add form wrapper.
 */
function rules_routine_add_form_wrapper($bundle) {
  $routine = entity_create('rules_routine', array('type' => $bundle));
  return drupal_get_form('rules_routine_form', $routine, 'create');
}

/**
 * Rules routine form.
 */
function rules_routine_form($form, &$form_state, $routine, $op) {
  form_load_include($form_state, 'inc', 'rules_routine', 'rules_routine.admin');
  $form_state['op'] = $op;

  if ($op === 'clone' && empty($form_state['routine'])) {
    $routine->is_new = TRUE;
    unset($routine->id);
    $routine->label = t('Clone of !label', array('!label' => $routine->label));
    $routine->name = $routine->name . '_clone';
  }

  $form_state['routine'] = $routine;

  if ($op == 'delete') {
    $form['message'] = array(
      '#markup' => t('Are you sure you want to delete this routine? <em>This action cannot be undone</em>'),
    );

    $form['actions'] = array(
      '#type' => 'actions',
    );
    $form['actions']['delete'] = array(
      '#type' => 'submit',
      '#value' => t('Delete'),
      '#submit' => array(
        'rules_routine_form_delete_submit',
      ),
    );

    return $form;
  }
  else if ($op == 'export') {
    $form['export'] = array(
      '#title' => t('Export'),
      '#type' => 'textarea',
      '#rows' => 30,
      '#default_value' => entity_export('rules_routine', $routine),
    );

    return $form;
  }

  // Display message for helpful information.
  if (empty($routine->routine_datetime[LANGUAGE_NONE])) {
    if (!$routine->is_new) {
      drupal_set_message(
        t(
          'This routine expired on @date, fill out the form below to reschedule its execution.',
          array(
            '@date' => $routine->formatFinal(),
          )
        ),
        'error'
      );

      // If we have a historical rrule stored, add a helpfule default value to
      // the routine.
      if (!empty($routine->rrule)) {
        $routine->routine_datetime[LANGUAGE_NONE][0] = array(
          'value' => '',
          'rrule' => $routine->rrule,
          'date_type' => 'datetime',
        );
      }
    }
  }
  else if (count($routine->routine_datetime[LANGUAGE_NONE]) == 1) {
    drupal_set_message(
      t(
        'This routine will expire on its next execution (@date) at which time it is !auto to auto-reschedule.',
        array(
          '@date' => $routine->formatNext(),
          '!auto' => $routine->auto_renew ? t('set') : t('NOT set'),
        )
      ),
      'warning'
    );
  }
  else {
    drupal_set_message(
      t(
        'This routine is scheduled for @count executions and will expire on @final at which time it is !auto to auto-reschedule.<br />This routine will run next on @date.',
        array(
          '@count' => count($routine->routine_datetime[LANGUAGE_NONE]),
          '@final' => $routine->formatFinal(),
          '@date' => $routine->formatNext(),
          '!auto' => $routine->auto_renew ? t('set') : t('NOT set'),
        )
      )
    );
  }

  $form['label'] = array(
    '#title' => t('Label'),
    '#type' => 'textfield',
    '#default_value' => $routine->label,
    '#description' => t('A label for this routine.'),
    '#required' => TRUE,
    '#size' => 30,
  );

  $form['name'] = array(
    '#type' => 'machine_name',
    '#default_value' => isset($routine->name) ? $routine->name : '',
    '#maxlength' => 255,
    '#disabled' => !$routine->is_new && $op != 'clone',
    '#machine_name' => array(
      'exists' => 'rules_routine_load',
      'source' => array('label'),
    ),
    '#description' => t('A unique machine name for this routine.'),
  );

  $form['component'] = array(
    '#type' => 'select',
    '#title' => t('Component'),
    '#description' => t('What component should be executed'),
    '#options' => entity_get_controller('rules_routine')->getComponentOptions($routine),
    '#default_value' => !empty($routine->component) ? $routine->component : '',
    '#required' => TRUE,
  );

  field_attach_form('rules_routine', $routine, $form, $form_state);

  $form['actions'] = array(
    '#type' => 'actions',
  );
  $form['actions']['save'] = array(
    '#type' => 'submit',
    '#value' => t('Save'),
  );

  return $form;
}

/**
 * Validate routine form.
 */
function rules_routine_form_validate($form, &$form_state) {
  $first_time = $form_state['values']['routine_datetime']['und'][0]['value'];
  if ($first_time < gmdate('Y-m-d H:i:s', REQUEST_TIME)) {
    form_error($form['routine_datetime']['und'][0]['value'], t('Cannot schedule a routine to start in the past.'));
  }
  field_attach_form_validate('rules_routine', $form_state['routine'], $form, $form_state);

  if (
    ($form_state['op'] === 'clone' || $form_state['routine']->is_new) &&
    rules_routine_load($form_state['values']['name'])
  ) {
    form_error($form['name'], 'A routine with that name already exists; the routine name must be unique.');
  }
}

/**
 * Submit routine delete button.
 */
function rules_routine_form_delete_submit($form, &$form_state) {
  $routine = $form_state['routine'];
  entity_delete('rules_routine', $routine->id);

  $form_state['redirect'] = 'admin/config/workflow/rules/routines';
}

/**
 * Submit routine form.
 */
function rules_routine_form_submit($form, &$form_state) {
  $routine = $form_state['routine'];
  $routine->label = $form_state['values']['label'];
  $routine->name = $form_state['values']['name'];
  $routine->component = $form_state['values']['component'];
  $routine->auto_renew = !empty($form_state['values']['auto_renew']);
  if ($routine->status == 'failed' || $routine->status == 'expired') {
    $routine->status = 'scheduled';
  }
  field_attach_submit('rules_routine', $routine, $form, $form_state);
  $routine->save();

  if ($form_state['op'] === 'clone') {
    $form_state['redirect'] = 'admin/config/workflow/rules/routines/routine/'.$routine->id;
  }
}
